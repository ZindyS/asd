package com.example.myapplication;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

public class Main3Activity extends AppCompatActivity {

    SharedPreferences sh;
    ConstraintLayout conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        sh = getSharedPreferences("Bruh", 0);
        conn = findViewById(R.id.connnnnnnn);
        conn.setBackground(Drawable.createFromPath(sh.getString("Bred", "")));
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitle(sh.getString("Label", "Error404"));
    }
}

package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {

    ConstraintLayout con;
    SharedPreferences sh;
    SharedPreferences.Editor ed;
    EditText text;
    Button continue1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        con = findViewById(R.id.con);
        sh = getSharedPreferences("Bruh", 0);
        con.setBackground(Drawable.createFromPath(sh.getString("Bred", "")));
        text = findViewById(R.id.editText);
        continue1 = findViewById(R.id.button8);
        continue1.setClickable(false);
        while (true) {
            if (!TextUtils.isEmpty(text.getText())) {
                continue1.setClickable(true);
                break;
            }
        }
    }

    public void OnHehCl(View v) {
        ed.putString("Label", String.valueOf(text.getText()));
        ed.putInt("stat", 1);
        ed.commit();
        Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
        startActivity(intent);
    }
}

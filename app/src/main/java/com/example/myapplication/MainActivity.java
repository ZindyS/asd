package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sh;
    SharedPreferences.Editor ed;
    Button one,two, three, four, five, six, continue1;
    Drawable vid;

    Drawable onew, twow, theew, fourw, fivew, sixw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sh = getSharedPreferences("Bruh", 0);
        ed = sh.edit();
        if (sh.getInt("stat", 0) == 1) {
            Intent intent = new Intent(MainActivity.this, Main3Activity.class);
            startActivity(intent);
        }
        one = findViewById(R.id.button);
        two = findViewById(R.id.button2);
        three = findViewById(R.id.button3);
        four = findViewById(R.id.button4);
        five = findViewById(R.id.button5);
        six = findViewById(R.id.button6);
        continue1 = findViewById(R.id.button7);
        onew = one.getBackground();
        twow = two.getBackground();
        theew = three.getBackground();
        fourw = four.getBackground();
        fivew = five.getBackground();
        sixw = six.getBackground();
        continue1.setClickable(false);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = onew;
                two.setBackground(twow);
                three.setBackground(theew);
                four.setBackground(fourw);
                five.setBackground(fivew);
                six.setBackground(sixw);
                continue1.setClickable(true);
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = twow;
                one.setBackground(onew);
                three.setBackground(theew);
                four.setBackground(fourw);
                five.setBackground(fivew);
                six.setBackground(sixw);
                continue1.setClickable(true);
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = theew;
                two.setBackground(twow);
                one.setBackground(onew);
                four.setBackground(fourw);
                five.setBackground(fivew);
                six.setBackground(sixw);
                continue1.setClickable(true);
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = fourw;
                two.setBackground(twow);
                three.setBackground(theew);
                one.setBackground(onew);
                five.setBackground(fivew);
                six.setBackground(sixw);
                continue1.setClickable(true);
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = fivew;
                two.setBackground(twow);
                three.setBackground(theew);
                four.setBackground(fourw);
                one.setBackground(onew);
                six.setBackground(sixw);
                continue1.setClickable(true);
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vid = sixw;
                two.setBackground(twow);
                three.setBackground(theew);
                four.setBackground(fourw);
                five.setBackground(fivew);
                one.setBackground(onew);
                continue1.setClickable(true);
            }
        });
    }

    public void onBruhCl(View v) {
        ed.putString("Bred", String.valueOf(vid));
        ed.commit();
        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(intent);
    }
}
